import {RouterModule} from '@angular/router';
import {NgModule} from '@angular/core';
import {DashboardDemoComponent} from './demo/view/dashboarddemo.component';
import {FormLayoutDemoComponent} from './demo/view/formlayoutdemo.component';
import {PanelsDemoComponent} from './demo/view/panelsdemo.component';
import {OverlaysDemoComponent} from './demo/view/overlaysdemo.component';
import {MenusDemoComponent} from './demo/view/menusdemo.component';
import {MessagesDemoComponent} from './demo/view/messagesdemo.component';
import {MiscDemoComponent} from './demo/view/miscdemo.component';
import {EmptyDemoComponent} from './demo/view/emptydemo.component';
import {ChartsDemoComponent} from './demo/view/chartsdemo.component';
import {FileDemoComponent} from './demo/view/filedemo.component';
import {DocumentationComponent} from './demo/view/documentation.component';
import {AppMainComponent} from './app.main.component';
import {AppNotfoundComponent} from './pages/app.notfound.component';
import {AppErrorComponent} from './pages/app.error.component';
import {AppAccessdeniedComponent} from './pages/app.accessdenied.component';
import {AppLoginComponent} from './pages/app.login.component';
import {InputDemoComponent} from './demo/view/inputdemo.component';
import {ButtonDemoComponent} from './demo/view/buttondemo.component';
import {TableDemoComponent} from './demo/view/tabledemo.component';
import {ListDemoComponent} from './demo/view/listdemo.component';
import {TreeDemoComponent} from './demo/view/treedemo.component';
import {DisplayComponent} from './utilities/display.component';
import {ElevationComponent} from './utilities/elevation.component';
import {FlexboxComponent} from './utilities/flexbox.component';
import {GridComponent} from './utilities/grid.component';
import {IconsComponent} from './utilities/icons.component';
import {WidgetsComponent} from './utilities/widgets.component';
import {SpacingComponent} from './utilities/spacing.component';
import {TypographyComponent} from './utilities/typography.component';
import {TextComponent} from './utilities/text.component';
import {AppCrudComponent} from './pages/app.crud.component';
import {AppCalendarComponent} from './pages/app.calendar.component';
import {AppInvoiceComponent} from './pages/app.invoice.component';
import {AppHelpComponent} from './pages/app.help.component';
import { ProddashboardComponent } from './demo/proddashboard/proddashboard.component';
import { ProjectlistComponent } from './demo/projectlist/projectlist.component';
import { ProddetailsComponent } from './demo/proddetails/proddetails.component';

import { CreatedocComponent } from './demo/createdoc/createdoc.component';
import { MydocsComponent } from './demo/mydocs/mydocs.component';
import { WorkflowsComponent } from './demo/workflows/workflows.component';
import { DocdetailsComponent } from './demo/docdetails/docdetails.component';
import { WorkflowapprovalComponent } from './demo/workflowapproval/workflowapproval.component';
import { WfacreateprofileComponent } from './demo/wfacreateprofile/wfacreateprofile.component';
import { ProfiledetailsComponent } from './demo/profiledetails/profiledetails.component';
import { CreateworkflowComponent } from './demo/createworkflow/createworkflow.component';
import { PhasedetailsComponent } from './demo/phasedetails/phasedetails.component';
import { PhaseapprovalComponent } from './demo/phaseapproval/phaseapproval.component';
import { ProjectphasedetailsComponent } from './demo/projectphasedetails/projectphasedetails.component';
import { ProjectdetailsComponent } from './demo/projectdetails/projectdetails.component';
import { PhasetreeComponent } from './demo/phasetree/phasetree.component';
import { ProjectdashboardComponent } from './demo/projectdashboard/projectdashboard.component';



@NgModule({
    imports: [
        RouterModule.forRoot([
            {
                path: '', component: AppMainComponent,
                children: [
                    {path: '', component: ProjectdashboardComponent},
                    {path: 'phasetree', component: PhasetreeComponent},
                    {path: 'projectdetails', component: ProjectdetailsComponent},
                    {path: 'projectlist', component: ProjectlistComponent},
                    {path: 'projectphasedetails', component: ProjectphasedetailsComponent},
                    {path: 'createworkflow', component: CreateworkflowComponent},
                    {path: 'phaseapproval', component: PhaseapprovalComponent},
                    {path: 'phasedetails', component: PhasedetailsComponent},
                    {path: 'profiledetails', component: ProfiledetailsComponent},
                    {path: 'wfacreateprofile', component: WfacreateprofileComponent},
                    {path: 'workflowapproval', component: WorkflowapprovalComponent},
                    {path: 'docdetails', component: DocdetailsComponent},
                    {path: 'mydocs', component: MydocsComponent},
                    {path: 'workflows', component: WorkflowsComponent},
                    {path: 'diamonddashboard', component: DashboardDemoComponent},
                    {path: 'createdoc', component: CreatedocComponent},
                  
                    {path: 'proddashboard', component: ProddashboardComponent},
                    {path: 'uikit/formlayout', component: FormLayoutDemoComponent},
                    {path: 'uikit/input', component: InputDemoComponent},
                    {path: 'uikit/button', component: ButtonDemoComponent},
                    {path: 'uikit/table', component: TableDemoComponent},
                    {path: 'uikit/list', component: ListDemoComponent},
                    {path: 'uikit/tree', component: TreeDemoComponent},
                    {path: 'uikit/panel', component: PanelsDemoComponent},
                    {path: 'uikit/overlay', component: OverlaysDemoComponent},
                    {path: 'uikit/menu', component: MenusDemoComponent},
                    {path: 'uikit/message', component: MessagesDemoComponent},
                    {path: 'uikit/misc', component: MiscDemoComponent},
                    {path: 'uikit/charts', component: ChartsDemoComponent},
                    {path: 'uikit/file', component: FileDemoComponent},
                    {path: 'utilities/display', component: DisplayComponent},
                    {path: 'utilities/elevation', component: ElevationComponent},
                    {path: 'utilities/flexbox', component: FlexboxComponent},
                    {path: 'utilities/grid', component: GridComponent},
                    {path: 'utilities/icons', component: IconsComponent},
                    {path: 'utilities/widgets', component: WidgetsComponent},
                    {path: 'utilities/spacing', component: SpacingComponent},
                    {path: 'utilities/typography', component: TypographyComponent},
                    {path: 'utilities/text', component: TextComponent},
                    {path: 'pages/crud', component: AppCrudComponent},
                    {path: 'pages/calendar', component: AppCalendarComponent},
                    {path: 'pages/invoice', component: AppInvoiceComponent},
                    {path: 'pages/help', component: AppHelpComponent},
                    {path: 'pages/empty', component: EmptyDemoComponent},
                    {path: 'documentation', component: DocumentationComponent}
                    
                ]
            },
            {path: 'error', component: AppErrorComponent},
            {path: 'access', component: AppAccessdeniedComponent},
            {path: 'notfound', component: AppNotfoundComponent},
            {path: 'login', component: AppLoginComponent},
            {path: '**', redirectTo: '/notfound'},
        ], {scrollPositionRestoration: 'enabled'})
    ],
    exports: [RouterModule]
})
export class AppRoutingModule {
}
