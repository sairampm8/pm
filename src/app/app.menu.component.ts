import {Component, OnInit} from '@angular/core';
import {AppMainComponent} from './app.main.component';

@Component({
    selector: 'app-menu',
    templateUrl: './app.menu.component.html',
})
export class AppMenuComponent implements OnInit {

    model: any[];

    constructor(public app: AppMainComponent) {}

    ngOnInit() {
        this.model = [
            {
                label: 'Dashboard', icon: 'pi pi-home',routerLink: ['/']
               
            },
            {separator: true},
            {
               
              label: 'Projects', icon: 'pi pi-fw pi-briefcase',routerLink: ['/projectlist']
               
            },
            {separator: true},
            {
               label: 'Tasks', icon: 'pi pi-fw pi-check-square',routerLink: ['/projectdetails']
           
            },

            {separator: true},
            {
                 label: 'Notifications', icon: 'pi pi-fw pi-bell',routerLink: ['/']
               
            },
           

            {separator: true},

            {
              label: 'Reports', icon: 'pi pi-fw pi-file', routerLink: ['/']
          
             },

          
            {separator: true},

           {
              label: 'Help', icon: 'pi pi-fw pi-info-circle', routerLink: ['/']
         
            },

     
            {separator: true},

      
            {
                label: "Admin",
                icon: "pi pi-fw pi-user",routerLink: ['/']
                // items: [
                // {
                // label: "Re Submit",
                // icon: "pi pi-fw pi-refresh",
                // },
                // {
                // label: "Manage Team",
                // icon: "pi pi-fw pi-users",
                // },
                // {
                // label: "Manage Approval",
                // icon: "pi pi-fw pi-check-circle",
                // },
                // {
                // label: "Templates",
                // icon: "pi pi-fw pi-tablet",
                // },
                // ],
                },
           
           
        ];
    }

    onMenuClick(event) {
        this.app.onMenuClick(event);
    }
}
