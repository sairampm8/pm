import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { WorkflowapprovalComponent } from './workflowapproval.component';

describe('WorkflowapprovalComponent', () => {
  let component: WorkflowapprovalComponent;
  let fixture: ComponentFixture<WorkflowapprovalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ WorkflowapprovalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WorkflowapprovalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
