import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PhasedetailsComponent } from './phasedetails.component';

describe('PhasedetailsComponent', () => {
  let component: PhasedetailsComponent;
  let fixture: ComponentFixture<PhasedetailsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PhasedetailsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PhasedetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
