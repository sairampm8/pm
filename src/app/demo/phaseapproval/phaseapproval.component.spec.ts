import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PhaseapprovalComponent } from './phaseapproval.component';

describe('PhaseapprovalComponent', () => {
  let component: PhaseapprovalComponent;
  let fixture: ComponentFixture<PhaseapprovalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PhaseapprovalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PhaseapprovalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
