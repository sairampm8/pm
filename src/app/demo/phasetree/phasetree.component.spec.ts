import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PhasetreeComponent } from './phasetree.component';

describe('PhasetreeComponent', () => {
  let component: PhasetreeComponent;
  let fixture: ComponentFixture<PhasetreeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PhasetreeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PhasetreeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
