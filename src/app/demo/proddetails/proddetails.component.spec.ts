
import {Component, OnInit} from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { ProddetailsComponent } from './proddetails.component';
import {MenuItem} from 'primeng/api';
import {BreadcrumbService} from '../../app.breadcrumb.service';


@Component({
  selector: 'app-proddetails',
  templateUrl: './proddetails.component.html',
  styleUrls: ['./proddetails.component.scss']
})

export class ButtonDemoComponent implements OnInit {

  items: MenuItem[];

  constructor(private breadcrumbService: BreadcrumbService) {
      this.breadcrumbService.setItems([
          {label: 'Button'}
      ]);
  }

  describe('ProddetailsComponent', () => {
    let component: ProddetailsComponent;
    let fixture: ComponentFixture<ProddetailsComponent>;
  
    beforeEach(async(() => {
      TestBed.configureTestingModule({
        declarations: [ ProddetailsComponent ]
      })
      .compileComponents();
    }));
  
    beforeEach(() => {
      fixture = TestBed.createComponent(ProddetailsComponent);
      component = fixture.componentInstance;
      fixture.detectChanges();
    });
  
    it('should create', () => {
      expect(component).toBeTruthy();
    });
  });
  

  ngOnInit() {
      this.items = [
          {label: 'Update', icon: 'pi pi-refresh'},
          {label: 'Delete', icon: 'pi pi-times'},
          {label: 'Angular.io', icon: 'pi pi-info', url: 'http://angular.io'},
          {separator: true},
          {label: 'Setup', icon: 'pi pi-cog'}
      ];
  }
}

