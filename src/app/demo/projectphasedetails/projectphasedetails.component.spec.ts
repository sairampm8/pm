import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProjectphasedetailsComponent } from './projectphasedetails.component';

describe('ProjectphasedetailsComponent', () => {
  let component: ProjectphasedetailsComponent;
  let fixture: ComponentFixture<ProjectphasedetailsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProjectphasedetailsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProjectphasedetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
