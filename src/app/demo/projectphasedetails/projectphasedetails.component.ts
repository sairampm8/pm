import {Component, OnInit, ViewChild} from '@angular/core';
import {Customer, Representative} from '../domain/customer';
import {CustomerService} from '../service/customerservice';
import {Product} from '../domain/product';
import {ProductService} from '../service/productservice';
import {Table} from 'primeng/table';
import {MegaMenuItem, MenuItem} from 'primeng/api';
import {BreadcrumbService} from '../../app.breadcrumb.service';
import {EventService} from '../service/eventservice';
import dayGridPlugin from '@fullcalendar/daygrid';
import timeGridPlugin from '@fullcalendar/timegrid';
import interactionPlugin from '@fullcalendar/interaction';




@Component({
  selector: 'app-projectphasedetails',
  templateUrl: './projectphasedetails.component.html',
  styleUrls: ['./projectphasedetails.component.scss'],

    styles: [`
		:host ::ng-deep .misc-demo .p-button.p-widget {
		    min-width: 6rem;
	    }

		:host ::ng-deep .misc-demo .badges .p-badge {
		    margin-right: .5rem;
		}

		:host ::ng-deep .misc-demo .badges .p-tag {
			margin-right: .5rem;
		}
    `]

})

export class ProjectphasedetailsComponent implements OnInit {

    customers1: Customer[];

    customers2: Customer[];

    customers3: Customer[];

    selectedCustomers1: Customer[];

    selectedCustomer: Customer;

    representatives: Representative[];

    statuses: any[];

    products: Product[];
  
    stepsItems: MenuItem[];

    events: any[];

    options: any;

    header: any;

    eventDialog: boolean;

    changedEvent: any;

    clickedEvent = null;

    rowGroupMetadata: any;
    public phaseOne=true;
    public phaseTwo=false;
    public phaseThree=false;
    public phaseFour=false;
    public phaseFive=false;
    @ViewChild('dt') table: Table;

    constructor(private customerService: CustomerService, private productService: ProductService, private eventService: EventService,
                private breadcrumbService: BreadcrumbService) {
        this.breadcrumbService.setItems([
            {label: 'Table'}
        ]);
    }



    ngOnInit() {
        this.customerService.getCustomersMedium().then(customers => this.customers1 = customers);
        this.customerService.getCustomersMedium().then(customers => this.customers2 = customers);
        this.customerService.getCustomersMedium().then(customers => this.customers3 = customers);
        this.productService.getProductsWithOrdersSmall().then(data => this.products = data);

        this.representatives = [
            {name: 'Amy Elsner', image: 'amyelsner.png'},
            {name: 'Anna Fali', image: 'annafali.png'},
            {name: 'Asiya Javayant', image: 'asiyajavayant.png'},
            {name: 'Bernardo Dominic', image: 'bernardodominic.png'},
            {name: 'Elwin Sharvill', image: 'elwinsharvill.png'},
            {name: 'Ioni Bowcher', image: 'ionibowcher.png'},
            {name: 'Ivan Magalhaes', image: 'ivanmagalhaes.png'},
            {name: 'Onyama Limba', image: 'onyamalimba.png'},
            {name: 'Stephen Shaw', image: 'stephenshaw.png'},
            {name: 'XuXue Feng', image: 'xuxuefeng.png'}
        ];

        this.statuses = [
            {label: 'Unqualified', value: 'unqualified'},
            {label: 'Qualified', value: 'qualified'},
            {label: 'New', value: 'new'},
            {label: 'Negotiation', value: 'negotiation'},
            {label: 'Renewal', value: 'renewal'},
            {label: 'Proposal', value: 'proposal'}
        ];

        this.stepsItems = [
            {
                label: 'Phase1'
            },
            {
                label: 'Phase2'
            },
            {
                label: 'Phase3'
            },
            {
                label: 'Phase4'
            },
            {
                label: 'Phase5'
            }
        ];

        this.eventService.getEvents().then(events => {this.events = events; });
        this.changedEvent = {title: '', start: null, end: '', allDay: null};

        this.options = {
            plugins: [ dayGridPlugin, timeGridPlugin, interactionPlugin ],
            defaultDate: '2017-02-01',
            header: {
                left: 'prev,next',
                center: 'title',
                right: 'dayGridMonth,timeGridWeek,timeGridDay'
            },
            editable: true,
            eventClick: (e) => {
                this.eventDialog = true;

                this.clickedEvent = e.event;

                this.changedEvent.title = this.clickedEvent.title;
                this.changedEvent.start = this.clickedEvent.start;
                this.changedEvent.end = this.clickedEvent.end;
            }
        };

    }

    save() {
        this.eventDialog = false;

        this.clickedEvent.setProp('title', this.changedEvent.title);
        this.clickedEvent.setStart(this.changedEvent.start);
        this.clickedEvent.setEnd(this.changedEvent.end);
        this.clickedEvent.setAllDay(this.changedEvent.allDay);

        this.changedEvent = {title: '', start: null, end: '', allDay: null};
    }

    reset() {
        this.changedEvent.title = this.clickedEvent.title;
        this.changedEvent.start = this.clickedEvent.start;
        this.changedEvent.end = this.clickedEvent.end;
    }





    onSort() {
        this.updateRowGroupMetaData();
    }

    updateRowGroupMetaData() {
        this.rowGroupMetadata = {};

        if (this.customers3) {
            for (let i = 0; i < this.customers3.length; i++) {
                const rowData = this.customers3[i];
                const representativeName = rowData.representative.name;

                if (i === 0) {
                    this.rowGroupMetadata[representativeName] = { index: 0, size: 1 };
                }
                else {
                    const previousRowData = this.customers3[i - 1];
                    const previousRowGroup = previousRowData.representative.name;
                    if (representativeName === previousRowGroup) {
                        this.rowGroupMetadata[representativeName].size++;
                    }
                    else {
                        this.rowGroupMetadata[representativeName] = { index: i, size: 1 };
                    }
                }
            }
        }
    }
    phases(phase){
        debugger
if(phase==1){
this.phaseOne=true;
this.phaseTwo=false;
this.phaseThree=false;
this.phaseFour=false;
this.phaseFive=false;
}
if(phase==2){
    this.phaseOne=false;
    this.phaseTwo=true;
    this.phaseThree=false;
    this.phaseFour=false;
    this.phaseFive=false;
}
if(phase==3){
    this.phaseOne=false;
this.phaseTwo=false;
this.phaseThree=true;
this.phaseFour=false;
this.phaseFive=false;

}
if(phase==4){
    this.phaseOne=false;
    this.phaseTwo=false;
    this.phaseThree=false;
    this.phaseFour=true;
    this.phaseFive=false;
}
if(phase==5){
    this.phaseOne=false;
    this.phaseTwo=false;
    this.phaseThree=false;
    this.phaseFour=false;
    this.phaseFive=true;
}

    }
}










   

