import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProddashboardComponent } from './proddashboard.component';

describe('ProddashboardComponent', () => {
  let component: ProddashboardComponent;
  let fixture: ComponentFixture<ProddashboardComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProddashboardComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProddashboardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
